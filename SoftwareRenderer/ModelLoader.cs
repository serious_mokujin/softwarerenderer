using System;
using System.Globalization;
using System.IO;
using System.Text.RegularExpressions;

namespace SoftwareRenderer
{
    class ModelLoader
    {
        public static Model LoadCOLLADA(string path)
        {
            Model m = new Model();

            //XmlDocument xmlDoc = new XmlDocument();
            //xmlDoc.Load(path);
            //XmlNode root = xmlDoc.DocumentElement;
            ////root.GetNamespaceOfPrefix()
            ////XmlNamespaceManager nsmgr = new XmlNamespaceManager(doc.NameTable);
            //nsmgr.AddNamespace("ns", "http://www.collada.org/2005/11/COLLADASchema");

            //XmlNode geomNode = xmlDoc.DocumentElement.GetE("library_geometries");///library_geometries");


            return m;
        }

        public static Model LoadFromSomeShittyUnknownFormat(string path)
        {
            Model m = new Model();
            Regex r = new Regex(@"\[[^a-z]*\]");
            string[] lines = File.ReadAllLines(path);
            int currentVertexParseIndex = 0;
            int currentIndexParseFace = 0;
            Vector3[] posArray = null;
            Vector3[] normalArray = null;
            Vector2[] uvArray = null;
            Vector3i[] facesArray = null;
            bool parseVertices = true;
            for(int i = 0; i < lines.Length; i++)
            {
                if( lines[i].StartsWith("NrVertices"))
                {
                    int l = int.Parse(lines[i].Split(':')[1]);
                    posArray = new Vector3[l];
                    normalArray = new Vector3[l];
                    uvArray = new Vector2[l];
                    parseVertices = true;
                }
                else if (lines[i].StartsWith("NrIndices"))
                {
                    int l = int.Parse(lines[i].Split(':')[1]);
                    facesArray = new Vector3i[(int)Math.Ceiling(l/3.0)];
                    parseVertices = false;
                }
                else
                {
                    if (parseVertices)
                    {
                        MatchCollection matches = r.Matches(lines[i]);
                        string positionInfo = matches[0].Value;
                        string normalInfo = matches[1].Value;
                        string binormalInfo = matches[2].Value;
                        string tanInfo = matches[3].Value;
                        string uvInfo = matches[4].Value;

                        string[] parts = null;
                        parts = positionInfo.Substring(1, positionInfo.Length - 2).Split(',');
                        Vector3 positionVec = new Vector3(  float.Parse(parts[0], CultureInfo.InvariantCulture), 
                                                            float.Parse(parts[1], CultureInfo.InvariantCulture), 
                                                            float.Parse(parts[2], CultureInfo.InvariantCulture));
                        
                        parts = normalInfo.Substring(1, normalInfo.Length - 2).Split(',');
                        Vector3 normalVec = new Vector3(float.Parse(parts[0], CultureInfo.InvariantCulture),
                                                            float.Parse(parts[1], CultureInfo.InvariantCulture),
                                                            float.Parse(parts[2], CultureInfo.InvariantCulture));

                        parts = uvInfo.Substring(1, uvInfo.Length - 2).Split(',');
                        Vector2 uvVec = new Vector2(float.Parse(parts[0], CultureInfo.InvariantCulture),
                                                            float.Parse(parts[1], CultureInfo.InvariantCulture));

                        posArray[currentVertexParseIndex] = positionVec;
                        normalArray[currentVertexParseIndex] = normalVec;
                        uvArray[currentVertexParseIndex] = uvVec;
                        currentVertexParseIndex++;
                    }
                    else
                    {
                        string[] parts = lines[i].Split('.')[1].Split(',');
                        facesArray[currentIndexParseFace++] = new Vector3i(int.Parse(parts[0]), int.Parse(parts[1]), int.Parse(parts[2]));
                    }
                }
            }
            m.vertices = posArray;
            m.normals = normalArray;
            m.faces = facesArray;
            m.texture = uvArray;
            return m;
        }
    }
}
