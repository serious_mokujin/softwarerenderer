using System;

namespace SoftwareRenderer
{
    class Model
    {
        Vector3[] m_posBuf;
        Vector3[] m_normBuf;
        Vector2[] m_texture;

        Vector3i[] m_facesBuf;

        public Vector3[] vertices
        {
            get { return m_posBuf; }
            set { m_posBuf = value; }
        }

        public Vector3[] normals
        {
            get { return m_normBuf; }
            set { m_normBuf = value; }
        }

        public Vector2[] texture
        {
            get { return m_texture; }
            set { m_texture = value; }
        }

        public int numFaces
        {
            get { return m_facesBuf.Length; }
        }

        public Vector3i[] faces
        {
            get { return m_facesBuf; }
            set { m_facesBuf = value; }
        }

        public Model()
        {
            m_posBuf = null;
            m_normBuf = null;
            m_facesBuf = null;
        }

        private void FindBounds(out Vector3 min, out Vector3 max)
        {
            max = m_posBuf[0];
            min = m_posBuf[0];
            for (int i = 1; i < m_posBuf.Length; i++)
            {
                if (max.x < m_posBuf[i].x)
                    max.x = m_posBuf[i].x;
                if (max.y < m_posBuf[i].y)
                    max.y = m_posBuf[i].y;
                if (max.z < m_posBuf[i].z)
                    max.z = m_posBuf[i].z;

                if (min.x > m_posBuf[i].x)
                    min.x = m_posBuf[i].x;
                if (min.y > m_posBuf[i].y)
                    min.y = m_posBuf[i].y;
                if (min.z > m_posBuf[i].z)
                    min.z = m_posBuf[i].z;
            }
        }

        public void MoveToGeometryCenter()
        {
            Vector3 max;
            Vector3 min;
            FindBounds(out min, out max);
            Vector3 shift = (max - min) * 0.5f;
            for (int i = 0; i < m_posBuf.Length; i++)
            {
                m_posBuf[i] = m_posBuf[i] - shift;
            }
        }

        public void NormalizePositions()
        {
            Vector3 max;
            Vector3 min;            
            FindBounds(out min, out max);

            float mag = Math.Max(min.Magnitude, max.Magnitude);

            for (int i = 0; i < m_posBuf.Length; i++)
            {
                m_posBuf[i] = m_posBuf[i]* (1.0f/mag);
            }
        }
    }
}
