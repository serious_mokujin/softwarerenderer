using System.Drawing;

namespace SoftwareRenderer
{
    class DemonstrationScene
    {
        private Model m_demoModel;
        private Bitmap m_texture;

        public Model Model
        {
            get
            {
                return m_demoModel;
            }
        }

        public Bitmap Texture
        {
            get
            {
                return m_texture;
            }
        }

        public float Scale { get; set; }
        public float Rotation { get; set; }
        public Vector3 LightDirection { get; set; }

        public DemonstrationScene()
        {
            m_demoModel = null;
            m_texture = null;

            Scale = 1.0f;
            Rotation = 0.0f;
            LightDirection = new Vector3(0.0f, 0.0f, 1.0f);
        }

        public void LoadModel(string filename)
        {
            m_demoModel = ModelLoader.LoadFromSomeShittyUnknownFormat(filename);
            m_demoModel.NormalizePositions();
        }

        public void LoadTexture(string filename)
        {
            m_texture = new Bitmap(filename);
        }
    }
}
