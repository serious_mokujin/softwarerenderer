﻿using System;

namespace SoftwareRenderer
{
    class Color
    {
        byte m_r;
        byte m_g;
        byte m_b;

        public byte R { get { return m_r; } set { m_r = value; } }
        public byte G { get { return m_g; } set { m_g = value; } }
        public byte B { get { return m_b; } set { m_b = value; } }
        public Vector3 Value { get { return new Vector3((float)m_r / 255.0f, (float)m_g / 255.0f, (float)m_b / 255.0f); } }
        public Color(byte r, byte g, byte b)
        {
            this.m_r = r;
            this.m_g = g;
            this.m_b = b;
        }

        public Color(Vector3 value)
        {
            this.m_r = (byte)(value.x * 255);
            this.m_g = (byte)(value.y * 255);
            this.m_b = (byte)(value.z * 255);
        }

        public Color(int argb)
        {
            byte[] bytes = BitConverter.GetBytes(argb);
            this.m_r = bytes[2];
            this.m_g = bytes[1];
            this.m_b = bytes[0];
        }

        public static implicit operator Color(Vector3 v)
        {
            return new Color(v);
        }
    }
}
