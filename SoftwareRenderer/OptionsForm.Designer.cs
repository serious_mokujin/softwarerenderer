namespace SoftwareRenderer
{
    partial class OptionsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.optionsGroupBox = new System.Windows.Forms.GroupBox();
            this.depthRadioButton = new System.Windows.Forms.RadioButton();
            this.colorRadioButton = new System.Windows.Forms.RadioButton();
            this.modelChooseBox = new System.Windows.Forms.ListBox();
            this.BackfaceCullCheckBox = new System.Windows.Forms.CheckBox();
            this.perspectiveScrollBar = new System.Windows.Forms.HScrollBar();
            this.label1 = new System.Windows.Forms.Label();
            this.optionsGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // optionsGroupBox
            // 
            this.optionsGroupBox.Controls.Add(this.depthRadioButton);
            this.optionsGroupBox.Controls.Add(this.colorRadioButton);
            this.optionsGroupBox.Location = new System.Drawing.Point(150, 12);
            this.optionsGroupBox.Name = "optionsGroupBox";
            this.optionsGroupBox.Size = new System.Drawing.Size(92, 71);
            this.optionsGroupBox.TabIndex = 5;
            this.optionsGroupBox.TabStop = false;
            this.optionsGroupBox.Text = "Output";
            // 
            // depthRadioButton
            // 
            this.depthRadioButton.AutoSize = true;
            this.depthRadioButton.Location = new System.Drawing.Point(6, 18);
            this.depthRadioButton.Name = "depthRadioButton";
            this.depthRadioButton.Size = new System.Drawing.Size(54, 17);
            this.depthRadioButton.TabIndex = 1;
            this.depthRadioButton.Text = "Depth";
            this.depthRadioButton.UseVisualStyleBackColor = true;
            this.depthRadioButton.CheckedChanged += new System.EventHandler(this.depthRadioButton_CheckedChanged);
            // 
            // colorRadioButton
            // 
            this.colorRadioButton.AutoSize = true;
            this.colorRadioButton.Checked = true;
            this.colorRadioButton.Location = new System.Drawing.Point(6, 41);
            this.colorRadioButton.Name = "colorRadioButton";
            this.colorRadioButton.Size = new System.Drawing.Size(49, 17);
            this.colorRadioButton.TabIndex = 2;
            this.colorRadioButton.TabStop = true;
            this.colorRadioButton.Text = "Color";
            this.colorRadioButton.UseVisualStyleBackColor = true;
            this.colorRadioButton.CheckedChanged += new System.EventHandler(this.colorRadioButton_CheckedChanged);
            // 
            // modelChooseBox
            // 
            this.modelChooseBox.FormattingEnabled = true;
            this.modelChooseBox.Items.AddRange(new object[] {
            "Witch",
            "Woman1",
            "Woman2",
            "Marine",
            "Goliath",
            "House",
            "Croco",
            "Bus",
            "Radar",
            "SCV",
            "Teran_Command_Center",
            "train"});
            this.modelChooseBox.Location = new System.Drawing.Point(12, 12);
            this.modelChooseBox.Name = "modelChooseBox";
            this.modelChooseBox.Size = new System.Drawing.Size(132, 147);
            this.modelChooseBox.TabIndex = 0;
            this.modelChooseBox.SelectedIndexChanged += new System.EventHandler(this.modelChooseBox_SelectedIndexChanged);
            // 
            // BackfaceCullCheckBox
            // 
            this.BackfaceCullCheckBox.AutoSize = true;
            this.BackfaceCullCheckBox.Checked = true;
            this.BackfaceCullCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.BackfaceCullCheckBox.Location = new System.Drawing.Point(150, 89);
            this.BackfaceCullCheckBox.Name = "BackfaceCullCheckBox";
            this.BackfaceCullCheckBox.Size = new System.Drawing.Size(90, 17);
            this.BackfaceCullCheckBox.TabIndex = 6;
            this.BackfaceCullCheckBox.Text = "backface cull";
            this.BackfaceCullCheckBox.UseVisualStyleBackColor = true;
            this.BackfaceCullCheckBox.CheckedChanged += new System.EventHandler(this.BackfaceCullCheckBox_CheckedChanged);
            // 
            // perspectiveScrollBar
            // 
            this.perspectiveScrollBar.Location = new System.Drawing.Point(147, 142);
            this.perspectiveScrollBar.Minimum = 10;
            this.perspectiveScrollBar.Name = "perspectiveScrollBar";
            this.perspectiveScrollBar.Size = new System.Drawing.Size(90, 17);
            this.perspectiveScrollBar.TabIndex = 7;
            this.perspectiveScrollBar.Value = 10;
            this.perspectiveScrollBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.perspectiveScrollBar_Scroll);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(147, 129);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Perspective";
            // 
            // OptionsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(253, 168);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.perspectiveScrollBar);
            this.Controls.Add(this.BackfaceCullCheckBox);
            this.Controls.Add(this.optionsGroupBox);
            this.Controls.Add(this.modelChooseBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "OptionsForm";
            this.Text = "OptionsForm";
            this.optionsGroupBox.ResumeLayout(false);
            this.optionsGroupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox optionsGroupBox;
        private System.Windows.Forms.ListBox modelChooseBox;
        private System.Windows.Forms.RadioButton depthRadioButton;
        private System.Windows.Forms.RadioButton colorRadioButton;
        private System.Windows.Forms.CheckBox BackfaceCullCheckBox;
        private System.Windows.Forms.HScrollBar perspectiveScrollBar;
        private System.Windows.Forms.Label label1;
    }
}