using System;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;

namespace SoftwareRenderer
{
    public partial class Form1 : Form
    {
        bool m_appRunning;
        Thread graphicsThread;
        Renderer renderer;
        Bitmap bufferBitmap;

        BufferedGraphicsContext currentContext;
        BufferedGraphics myBuffer;
        object rendererMutex = new object();

        Random rnd = new Random();

        Vector2 mousePosition = new Vector2(0,0);

        Camera camera;

        int fps;
        int targetFps = 60;

        DemonstrationScene m_scene;
        DemonstrationOptions m_options;

        OptionsForm optionsForm = null;

        public Form1()
        {
            graphicsThread = new Thread(RenderLoop);
            currentContext = new BufferedGraphicsContext();
            renderer = new Renderer(this.ClientSize.Width, this.ClientSize.Height);
            camera = new Camera();

            InitializeComponent();
            
            m_scene = new DemonstrationScene();
            m_options = DemonstrationOptions.Default;
            ApplyDemonstrationOptions(m_options);

            camera.SetViewLookAt(new Vector3(-5, -3, -5), new Vector3(0, -0.3f, 0), new Vector3(0, -1, 0));
            camera.SetViewport(0, 0, ClientRectangle.Width, ClientRectangle.Height);
        }

        void RenderLoop()
        {
            Stopwatch s = new Stopwatch();
            s.Start();
            int frameCounter = 0;
            long lastFpsMeasure = 0;
            long lastMilliseconds = 0;

            long targetMilliseconds = 1000 / targetFps;

            while (m_appRunning)
            {
                int deltaMilliseconds = (int)(s.ElapsedMilliseconds - lastMilliseconds);
                lock (rendererMutex)
                {
                    Render(deltaMilliseconds);
                }
                lastMilliseconds = s.ElapsedMilliseconds;
                if (lastMilliseconds - lastFpsMeasure >= 1000)
                {
                    lastFpsMeasure = lastMilliseconds;
                    fps = frameCounter;
                    frameCounter = 0;
                }
                frameCounter++;
                // refresh form
                this.Invalidate();
                int restTime = (int)(targetMilliseconds - deltaMilliseconds);
                if (restTime > 0)
                {
                    Thread.Sleep(restTime);
                }
            }
        }

        float lerp(float a, float b, float t)
        {
            return a + (b - a) * t;
        }

        Vector3 transform(Vector3 vertex, Matrix m)
        {
            Vector4 v = new Vector4(vertex.x, vertex.y, vertex.z, 1.0f);
            v = m * v;
            return new Vector3(v.x / v.w, v.y / v.w, v.z / v.w);
        }
        
        void Render(int dt)
        {
            m_scene.Rotation += 0.05f;

            Matrix model = Matrix.CreateRotationY(m_scene.Rotation);

            Matrix finalTransform = camera.Viewport * camera.Projection * camera.View * model;

            renderer.Clear(new Color(105, 20, 145));
            renderer.SetTexture(m_scene.Texture);

            Vector3[] vertices = m_scene.Model.vertices;
            Vector3i[] faces = m_scene.Model.faces;
            Vector2[] texCoord = m_scene.Model.texture;
            for (int i = 0; i < faces.Length; i++)
            {
                Vector3i face = faces[i];
                Vector3 v1 = -vertices[face.x];
                Vector3 v2 = -vertices[face.y];
                Vector3 v3 = -vertices[face.z];

                v1 = transform(v1, finalTransform);
                v2 = transform(v2, finalTransform);
                v3 = transform(v3, finalTransform);

                Vector2 uv1 = texCoord[face.x];
                Vector2 uv2 = texCoord[face.y];
                Vector2 uv3 = texCoord[face.z];

                Vector3 normal = Vector3.Normalize(Vector3.Cross(v2 - v1, v3 - v1));
                float lightIntencity = Math.Max(0, Vector3.Dot(m_scene.LightDirection, normal));
                
                if (normal.z < 0 && m_options.backfaceCull || !m_options.backfaceCull)
                {
                    renderer.DrawTriangle(v1, v2, v3, new Color((byte)(255.0f * lightIntencity),
                                                                (byte)(255.0f * lightIntencity),
                                                                (byte)(255.0f * lightIntencity)), uv1, uv2, uv3, lightIntencity, m_options.output == OutputValue.Depth);
                }
            }

            renderer.SwapBuffers();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            RefreshSize(this.ClientSize.Width, this.ClientSize.Height);
            m_appRunning = true;
            graphicsThread.Start();
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            BitmapData data = bufferBitmap.LockBits(new Rectangle(0, 0, bufferBitmap.Width, bufferBitmap.Height), ImageLockMode.WriteOnly, PixelFormat.Format24bppRgb);
            Marshal.Copy(renderer.FrontBuffer, 0, data.Scan0, renderer.Width * renderer.Height * renderer.BytesPerPixel);
            bufferBitmap.UnlockBits(data);
            myBuffer.Graphics.DrawImage(bufferBitmap, Point.Empty);
            myBuffer.Render(e.Graphics);
        }

        protected override void OnPaintBackground(PaintEventArgs pevent)
        { /* Do Nothing */ }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            m_appRunning = false;
        }
        
        protected override void OnSizeChanged(EventArgs e)
        {
            RefreshSize(this.ClientSize.Width, this.ClientSize.Height);
            base.OnSizeChanged(e);
        }

        void RefreshSize(int w, int h)
        {
            lock (rendererMutex)
            {
                myBuffer = currentContext.Allocate(CreateGraphics(), new Rectangle(0, 0, w, h));
                bufferBitmap = new Bitmap(w, h, PixelFormat.Format24bppRgb);
                renderer.Resize(w, h);
            }
        }

        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            float nx = 1.0f - ((float)e.X / (float)ClientRectangle.Width) * 2.0f;
            float ny = 1.0f - ((float)e.Y / (float)ClientRectangle.Height) * 2.0f;
            m_scene.LightDirection = Vector3.Normalize(new Vector3(nx, ny, -1));
            mousePosition = new Vector2(e.X, e.Y);
        }

        private void hScrollBar1_Scroll(object sender, ScrollEventArgs e)
        {
            m_scene.Scale = (float)hScrollBar1.Value;
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if( e.KeyCode == Keys.Tab )
            {
                if( optionsForm == null || !optionsForm.Visible)
                {
                    optionsForm = new OptionsForm(m_options);
                    optionsForm.OnDemonstrationOptionsChanged += optionsForm_OnDemonstrationOtionsChanged;
                    optionsForm.Show();
                }
                else
                {
                    optionsForm.Focus();
                }
            }
        }

        void optionsForm_OnDemonstrationOtionsChanged(DemonstrationOptions options)
        {
            ApplyDemonstrationOptions(options);
        }

        void ApplyDemonstrationOptions(DemonstrationOptions options)
        {
            m_options = options;
            string modelFilePath = "Resources/Models/" + options.modelFileName + ".nfg";
            string textureFilePath = "Resources/Textures/" + options.modelTextureName + ".png";
            m_scene.LoadModel(modelFilePath);
            m_scene.LoadTexture(textureFilePath);
            camera.SetPerspectiveProjection(0, m_options.perspective);
        }

        private void StatsUpdateTimer_Tick(object sender, EventArgs e)
        {
            fpsLabel.Text = "fps: " + fps.ToString();
        }
    }
}
