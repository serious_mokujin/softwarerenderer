using System;

namespace SoftwareRenderer
{
    // 0  1  2  3
    // 4  5  6  7
    // 8  9  10 11
    // 12 13 14 15
    //
    public class Matrix
    {
        public float[] m;

        public Matrix()
        {
            m = new float[16];
            for(int i = 0; i<16; i++)
            {
                m[i] = 0.0f;
            }
        }

        public void Set(int row, int col, float value)
        {
            m[row * 4 + col] = value;
        }

        public static Matrix CreateIdentity()
        {
            Matrix mat = new Matrix();
            mat.m[0] = mat.m[5] = mat.m[10] = mat.m[15] = 1;
            return mat;
        }

        public static Matrix CreateSimplePerspectiveZ(float distance)
        {
            Matrix m = CreateIdentity();
            m.m[14] = -1.0f / distance;

            return m;
        }

        public static Matrix CreateTranslation(float x, float y, float z)
        {
            Matrix m = CreateIdentity();
            m.m[3] = x;
            m.m[7] = y;
            m.m[11] = z;
            return m;
        }

        public static Matrix CreateScale(float x, float y, float z)
        {
            Matrix m = CreateIdentity();
            m.m[0] = x;
            m.m[5] = y;
            m.m[10] = z;
            return m;
        }

        public static Matrix CreateRotationY(float angle)
        {
            Matrix m = CreateIdentity();

            m.m[0] = (float)Math.Cos(angle);
            m.m[2] = -(float)Math.Sin(angle);
            m.m[8] = (float)Math.Sin(angle);
            m.m[10] = (float)Math.Cos(angle);

            return m;
        }

        public static Matrix operator * (Matrix a, Matrix b)
        {
            Matrix c = new Matrix();
            for(int i = 0; i<4;i++)
            {
                for(int j = 0; j < 4; j++)
                {
                    float sum = 0;
                    for (int k = 0; k < 4; k++)
                    {
                        sum += a.m[i * 4 + k] * b.m[k * 4 + j];
                    }
                    c.m[i * 4 + j] = sum;
                }
            }
            return c;
        }

        public static Vector4 operator * (Matrix m, Vector4 v)
        {
            return new Vector4( m.m[0] * v.x + m.m[1] * v.y + m.m[2] * v.z + m.m[3] * v.w,
                                m.m[4] * v.x + m.m[5] * v.y + m.m[6] * v.z + m.m[7] * v.w,
                                m.m[8] * v.x + m.m[9] * v.y + m.m[10] * v.z + m.m[11] * v.w,
                                m.m[12] * v.x + m.m[13] * v.y + m.m[14] * v.z + m.m[15] * v.w );
        }

        public override string ToString()
        {
            return string.Format("{0}  {1}  {2}  {3}\n{4}  {5}  {6}  {7}\n{8}  {9}  {10}  {11}\n{12}  {13}  {14}  {15} \n\n", m[0], m[1], m[2], m[3], m[4], m[5], m[6], m[7], m[8], m[9], m[10], m[11], m[12], m[13], m[14], m[15]);
        }
    }
}
