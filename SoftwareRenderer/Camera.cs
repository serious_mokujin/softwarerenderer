using System;

namespace SoftwareRenderer
{
    public class Camera
    {
        public Matrix View { get; private set; }
        public Matrix Projection { get; private set; }
        public Matrix Viewport { get; private set; }


        public Camera()
        {
        }

        public void SetViewLookAt(Vector3 eye, Vector3 center, Vector3 up)
        {
            Vector3 z = Vector3.Normalize(eye - center);
            Vector3 x = Vector3.Normalize(Vector3.Cross(z, up));
            Vector3 y = Vector3.Normalize(Vector3.Cross(z, x));
            Matrix minv = Matrix.CreateIdentity();
            Matrix translate = Matrix.CreateTranslation(-center.x, -center.y, -center.z);

            minv.Set(0, 0, x.x); minv.Set(0, 1, x.y); minv.Set(0, 2, x.z);
            minv.Set(1, 0, y.x); minv.Set(1, 1, y.y); minv.Set(1, 2, y.z);
            minv.Set(2, 0, z.x); minv.Set(2, 1, z.y); minv.Set(2, 2, z.z);

            View = minv * translate;
        }

        public void SetPerspectiveProjection(float near, float far)
        {
            Projection = Matrix.CreateSimplePerspectiveZ(far);
        }

        public void SetViewport(int x, int y, int width, int height)
        {
            Matrix vp = Matrix.CreateIdentity();

            vp.Set(0, 3, x + (float)width / 2.0f);
            vp.Set(1, 3, y + (float)height / 2.0f);
            vp.Set(2, 3, 0);

            vp.Set(0, 0, (float)width / 2.0f);
            vp.Set(1, 1, (float)height / 2.0f);
            vp.Set(2, 2, 1);
            Console.Write(vp.ToString());
            this.Viewport = vp;
        }
    }
}
