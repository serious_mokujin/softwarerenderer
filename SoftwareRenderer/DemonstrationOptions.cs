
namespace SoftwareRenderer
{
    public enum OutputValue
    {
        Color,
        Depth
    }

    public struct DemonstrationOptions
    {
        public OutputValue output;
        public string modelFileName;
        public string modelTextureName;

        public bool wireframe;
        public bool backfaceCull;

        public float perspective;

        public static DemonstrationOptions Default
        {
            get
            {
                DemonstrationOptions options;
                options.output = OutputValue.Color;
                options.modelFileName = "Marine";
                options.modelTextureName = "Marine";
                options.wireframe = false;
                options.backfaceCull = true;
                options.perspective = 2.0f;
                return options;
            }
        }
    }
}
