//#define UNSAFE_TEXTURE_FETCH

using System;
using System.Drawing;

namespace SoftwareRenderer
{
    class Renderer
    {
        const int bpp = 3;
        byte[] frontBuffer;
        byte[] backBuffer;
        float[] zBuffer;

        Bitmap texture;

        public byte[] FrontBuffer { get { return frontBuffer; } }

        int width, height;

        public int Width { get { return width; } }
        public int Height { get { return height; } }
        public int BytesPerPixel { get { return bpp; } }

        public Renderer(int width, int height)
        {
            Resize(width, height);
        }

        public void Resize(int w, int h)
        {
            this.width = w;
            this.height = h;

            frontBuffer = new byte[width * height * bpp];
            backBuffer = new byte[width * height * bpp];
            zBuffer = new float[width * height];
        }

        public void Clear(Color col)
        {
            for( int i = 0; i < width*height; i+=1 )
            {
                backBuffer[i*bpp] = col.R;
                backBuffer[i*bpp+1] = col.G;
                backBuffer[i*bpp+2] = col.B;
                zBuffer[i] = float.NegativeInfinity;
            }
            
        }

        void swap(ref int a, ref int b)
        {
            int t = a;
            a = b;
            b = t;
        }

        public void DrawLine(int x0, int y0, int x1, int y1, Color color)
        {
            bool transp = false;
            if( Math.Abs(y0-y1) > Math.Abs(x0-x1))
            {
                swap(ref x0, ref y0);
                swap(ref x1, ref y1);
                transp = true;
            }
            if( x0 > x1 )
            {
                swap(ref x0, ref x1);
                swap(ref y0, ref y1);
            }
            for( int x = x0; x <= x1; x++ )
            {
                float t = (x-x0)/(float)(x1-x0);
                int y = (int)(y0*(1.0f-t) + y1*t);
                if( transp )
                {
                    Set(y, x, color);
                }
                else
                {
                    Set(x, y, color);
                }
            }
        }

        public void DrawLines(Vector3[] points, Color color)
        {
            for ( int i = 1; i < points.Length; i++ )
            {
                Vector3 p1 = points[i-1];
                Vector3 p2 = points[i];
                DrawLine((int)p1.x, (int)p1.y, (int)p2.x, (int)p2.y, color);
            }
        }

        public void SetTexture(Bitmap texture)
        {
            this.texture = texture;
        }

        public unsafe void DrawTriangle(Vector3 v1, Vector3 v2, Vector3 v3, Color color, Vector2 uv1, Vector2 uv2, Vector2 uv3, float light, bool outputDepth)
        {
            int minX = (int)Math.Floor(Math.Min(v1.x, Math.Min(v2.x, v3.x)));
            int maxX = (int)Math.Ceiling(Math.Max(v1.x, Math.Max(v2.x, v3.x)));
            int minY = (int)Math.Floor(Math.Min(v1.y, Math.Min(v2.y, v3.y)));
            int maxY = (int)Math.Ceiling(Math.Max(v1.y, Math.Max(v2.y, v3.y)));

            minX = Math.Max(0, Math.Min(width, minX));
            minY = Math.Max(0, Math.Min(height, minY));

            maxX = Math.Max(0, Math.Min(width, maxX));
            maxY = Math.Max(0, Math.Min(height, maxY));

            Vector2 projectedV1 = new Vector2(v1.x, v1.y);
            Vector2 projectedV2 = new Vector2(v2.x, v2.y);
            Vector2 projectedV3 = new Vector2(v3.x, v3.y);
#if UNSAFE_TEXTURE_FETCH
            byte* textureBytes = (byte*)IntPtr.Zero.ToPointer();
            BitmapData bdata = null;
            if (!outputDepth)
            {
                bdata = texture.LockBits(new Rectangle(0, 0, texture.Size.Width, texture.Size.Height), System.Drawing.Imaging.ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppRgb);
                textureBytes = (byte*)bdata.Scan0.ToPointer();
            }
#endif
            for(int i = minX; i <= maxX; i++)
            {
                for(int j = minY; j < maxY; j++)
                {
                    Vector3 bary = getBarycentric(projectedV1, projectedV2, projectedV3, new Vector2(i, j));
                    if (bary.x < 0 || bary.y < 0 || bary.z < 0)
                    {
                        continue;
                    }

                    float depth = v1.z * bary.x + v2.z * bary.y + v3.z * bary.z;
                    if (TestDepth(i, j, depth))
                    {
                        SetDepth(i, j, depth);

                        if (outputDepth)
                        {
                            float depthNormal = depth * 200;
                            Set(i, j, new Color((byte)(Math.Max(0, Math.Min(255.0, depthNormal))),
                                (byte)(Math.Max(0, Math.Min(255, depthNormal))), (byte)(Math.Max(0, Math.Min(255, depthNormal)))));
                        }
                        else
                        {
                            Vector2 uv = uv1 * bary.x + uv2 * bary.y + uv3 * bary.z;
                            int texX = (int)(Math.Min(1.0f, uv.x) * (float)(texture.Width - 1));
                            int texY = (texture.Height - 1) - (int)(Math.Min(1.0f, uv.y) * (float)(texture.Height - 1));
#if UNSAFE_TEXTURE_FETCH
                            int offset = texY * texture.Width * 4 + texX * 4;
                            byte* ptrToPixel = (textureBytes + offset);
                            Color texColor = new Color(*(ptrToPixel + 2), *(ptrToPixel + 1), *(ptrToPixel + 0));
#else
                            Color texColor = new Color(texture.GetPixel(texX, texY).ToArgb());
#endif                      
                            texColor = texColor.Value * light;
                            Set(i, j, texColor);
                        }
                    }
                }
            }
#if UNSAFE_TEXTURE_FETCH
            if(bdata != null)
            {
                texture.UnlockBits(bdata);
            }
#endif
        }

        private void Set(int x, int y, Color color)
        {
            if (x < 0 || y < 0 || x >= width || y >= height)
                return;
            int index = (y * width + x) * bpp;
            backBuffer[index] = color.B;
            backBuffer[index+1] = color.G;
            backBuffer[index+2] = color.R;
        }

        private void SetDepth(int x, int y, float depth)
        {
            if (x < 0 || y < 0 || x >= width || y >= height)
                return;
            int index = (y * width + x);
            zBuffer[index] = depth;
        }
        
        private bool TestDepth(int x, int y, float depth)
        {
            if (x < 0 || y < 0 || x >= width || y >= height)
                return false;
            int index = (y * width + x);
            return zBuffer[index] < depth;
        }

        public Vector3 getBarycentric(Vector2 v1, Vector2 v2, Vector2 v3, Vector2 point)
        {
            float b1 = ((v2.y - v3.y)*(point.x - v3.x) + (v3.x - v2.x)*(point.y - v3.y)) / ((v2.y - v3.y)*(v1.x - v3.x) + (v3.x - v2.x)*(v1.y - v3.y));
            float b2 = ((v3.y - v1.y) * (point.x - v3.x) + (v1.x - v3.x) * (point.y - v3.y)) / ((v2.y - v3.y) * (v1.x - v3.x) + (v3.x - v2.x) * (v1.y - v3.y));
            float b3 = 1.0f - b1 - b2;
            return new Vector3(b1, b2, b3);
        }

        public void SwapBuffers()
        {
            byte[] temp = frontBuffer;
            frontBuffer = backBuffer;
            backBuffer = temp;
        }
    }
}
