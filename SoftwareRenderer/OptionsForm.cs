using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SoftwareRenderer
{
    public delegate void DemonstrationOptionsChangeHandler(DemonstrationOptions options);

    public partial class OptionsForm : Form
    {
        public event DemonstrationOptionsChangeHandler OnDemonstrationOptionsChanged;

        DemonstrationOptions m_options;

        public OptionsForm(DemonstrationOptions initialOptions)
        {
            InitializeComponent();

            m_options = initialOptions;
            InitWithOptions(initialOptions);
        }

        void InitWithOptions(DemonstrationOptions options)
        {
            modelChooseBox.SelectedItem = options.modelFileName;
            depthRadioButton.Checked = options.output == OutputValue.Depth;
            colorRadioButton.Checked = options.output == OutputValue.Color;
            BackfaceCullCheckBox.Checked = options.backfaceCull;
            perspectiveScrollBar.Value = (int)(options.perspective * 10);
        }

        private void modelChooseBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (modelChooseBox.SelectedItem != null)
            {
                m_options.modelFileName = modelChooseBox.SelectedItem.ToString();
                m_options.modelTextureName = modelChooseBox.SelectedItem.ToString();
            }

            if (OnDemonstrationOptionsChanged != null)
            {
                OnDemonstrationOptionsChanged(m_options);
            }
        }

        private void depthRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (depthRadioButton.Checked)
            {
                m_options.output = OutputValue.Depth;
            }

            if (OnDemonstrationOptionsChanged != null)
            {
                OnDemonstrationOptionsChanged(m_options);
            }
        }

        private void colorRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (colorRadioButton.Checked)
            {
                m_options.output = OutputValue.Color;
            }

            if (OnDemonstrationOptionsChanged != null)
            {
                OnDemonstrationOptionsChanged(m_options);
            }
        }

        private void BackfaceCullCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            m_options.backfaceCull = BackfaceCullCheckBox.Checked;

            if (OnDemonstrationOptionsChanged != null)
            {
                OnDemonstrationOptionsChanged(m_options);
            }
        }

        private void perspectiveScrollBar_Scroll(object sender, ScrollEventArgs e)
        {
            m_options.perspective = (float)perspectiveScrollBar.Value / 10.0f;

            if (OnDemonstrationOptionsChanged != null)
            {
                OnDemonstrationOptionsChanged(m_options);
            }
        }
    }
}
