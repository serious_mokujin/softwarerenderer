precision mediump float;
uniform samplerCube u_cubtex0;

varying vec3 v_reflection;

void main()
{
	gl_FragColor = textureCube(u_cubtex0,  v_reflection); 
}
