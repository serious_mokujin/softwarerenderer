uniform mat4 u_ModelViewProjMatrix;
uniform mat4 u_modelMatrix;
uniform float tilingFactor;

attribute vec3 a_pos;
attribute vec2 a_texcoord;

varying vec2 v_globalTexcoord;
varying vec2 v_tiledTexcoord;
varying vec3 v_position;

void main()
{
	v_globalTexcoord = a_texcoord;
	v_tiledTexcoord = a_texcoord * tilingFactor;
	v_position = vec3(u_modelMatrix * vec4(a_pos, 1.0));
	gl_Position = u_ModelViewProjMatrix *  vec4(a_pos, 1.0);
}
   