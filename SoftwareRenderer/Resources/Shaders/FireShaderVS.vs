uniform mat4 u_ModelViewProjMatrix;

attribute vec3 a_pos;
attribute vec2 a_texcoord;

varying vec2 v_texcoord;

void main()
{
	v_texcoord.xy = a_texcoord.xy;
	gl_Position = u_ModelViewProjMatrix *  vec4(a_pos, 1.0);
}
   