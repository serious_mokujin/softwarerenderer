precision mediump float;
uniform sampler2D u_tex0; // underlying
uniform sampler2D u_tex1; // normal map
uniform sampler2D u_tex2; // displacement map
uniform samplerCube u_cubtex0; // reflection map

uniform float u_time;
uniform vec3 u_cameraPos;


varying vec2 v_texcoord;
varying vec3 v_position;
varying vec3 v_normal;
varying vec3 v_binormal;
varying vec3 v_tangent;
varying float v_depth;

// parameters
uniform float FresnelPower;
uniform float wColorR;
uniform float wColorG;
uniform float wColorB;

// ----- MATERIAL -----------
uniform vec4 u_m_diffuse;
uniform vec4 u_m_specular;

// --------------------------
//----- LIGHT ---------------
uniform vec4 lightDiffuse[3];
uniform vec4 lightSpecular[3];
uniform vec4 lightPosition[3];

uniform int lightsCount;
//---------------------------
uniform float AmbientWeight;
uniform vec4 AmbientGlobal;
uniform float specularPower;
//-------------------------------

void main()
{
	vec2 disp = texture2D(u_tex2, vec2(v_texcoord.x, v_texcoord.y + u_time)).rg;
	vec2 offset = (2.0 * disp - 1.0)*v_depth;
	vec2 displaced_texcoords_N = v_texcoord + offset;

	disp = texture2D(u_tex2, vec2(v_texcoord.x + u_time, v_texcoord.y)).rg;
	offset = (2.0 * disp - 1.0) * v_depth;
	vec2 displaced_texcoords_C = v_texcoord + offset;

	vec3 normalT = texture2D(u_tex1, displaced_texcoords_N).xyz;
	mat3 tbn = mat3(normalize(v_tangent), normalize(v_binormal), normalize(v_normal));
	vec3 normal = normalize(tbn*(2.0 * normalT - 1.0));

	vec3 ToEye = normalize(u_cameraPos - v_position);
	
	float fresnel = pow(1.0 - abs(dot(normal, ToEye )), FresnelPower); // fresnel term
	
	vec4 colorBack = texture2D(u_tex0, displaced_texcoords_C);
	colorBack += vec4(wColorR,wColorG,wColorB, 1.0)*v_depth;
	vec4 reflectionColor = textureCube(u_cubtex0, normalize(reflect(-ToEye, normal))); 

	vec4 totalDiffuse = vec4(0.0, 0.0, 0.0, 0.0);
	vec4 totalSpecular = vec4(0.0, 0.0, 0.0, 0.0);

	for(int i = 0; i < lightsCount; i++)
	{
		vec4 diffuse = u_m_diffuse * lightDiffuse[i];
		vec4 specular = u_m_specular * lightSpecular[i];
		vec3 direction = lightPosition[i].w == 1.0 ? normalize(v_position - lightPosition[i].xyz) : lightPosition[i].xyz;
		
		vec4 diffuseComponent = max(dot(normal, -direction), 0.0)* diffuse;
		totalDiffuse += diffuseComponent;

		vec3 reflectVector = normalize(reflect(direction, normal));
		
		vec4 specularComponent = pow(max(dot(reflectVector, ToEye), 0.0), specularPower)* specular;
		totalSpecular += specularComponent;
	}

	gl_FragColor =  mix(vec4(((AmbientGlobal * AmbientWeight + totalDiffuse * (1.0 - AmbientWeight)) * colorBack + totalSpecular).xyz, 1.0), reflectionColor,  fresnel);
}
