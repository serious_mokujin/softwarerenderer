uniform mat4 u_ModelViewProjMatrix;
//uniform mat4 u_viewMatrix;
//uniform mat4 u_projMatrix;
//uniform mat4 u_modelMatrix;

attribute vec3 a_pos;

varying vec3 v_position;

void main()
{
	v_position = a_pos;
	gl_Position =  u_ModelViewProjMatrix *  vec4(a_pos, 1.0);
}
   