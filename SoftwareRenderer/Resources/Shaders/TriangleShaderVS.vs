uniform mat4 u_ModelViewProjMatrix;

attribute vec3 a_pos;
attribute vec3 a_col;

varying vec3 v_col;

void main()
{
	v_col = a_col;
	gl_Position = u_ModelViewProjMatrix *  vec4(a_pos, 1.0);

}
