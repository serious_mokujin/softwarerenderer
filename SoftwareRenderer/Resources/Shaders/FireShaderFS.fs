precision mediump float;
uniform sampler2D u_tex0; // fire texture
uniform sampler2D u_tex1; // displacement texture
uniform sampler2D u_tex2; // mask texture
uniform float dmax;
uniform float u_time;

varying vec2 v_texcoord;

void main()
{
	vec2 disp = texture2D(u_tex1, vec2(v_texcoord.x, v_texcoord.y + u_time)).rg;
	vec2 offset = (2.0 * disp - 1.0) * dmax;
	vec2 displaced_texcoords = v_texcoord + offset;
	vec4 fire_color = texture2D (u_tex0, displaced_texcoords);
	vec4 mask = texture2D(u_tex2, v_texcoord);
	float alpha = mask.r*mask.g*mask.b;
	gl_FragColor = fire_color * (1.0, 1.0, 1.0, alpha);
}
