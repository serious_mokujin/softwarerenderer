precision mediump float;
uniform sampler2D u_tex0; // blend tex
uniform sampler2D u_tex1; // grass
uniform sampler2D u_tex2; // stone
uniform sampler2D u_tex3; // dirt

uniform vec3 u_cameraPos;
uniform vec3 u_fogColor;
uniform float u_fogStart;
uniform float u_fogRange;

varying vec2 v_globalTexcoord;
varying vec2 v_tiledTexcoord;
varying vec3 v_position;

void main()
{
	vec3 blend_map = texture2D(u_tex0, v_globalTexcoord).rgb;
	vec3 grass = texture2D(u_tex2, v_tiledTexcoord).rgb;
	vec3 dirt = texture2D(u_tex1, v_tiledTexcoord).rgb;
	vec3 rock = texture2D(u_tex3, v_tiledTexcoord).rgb;
	
	float dist = distance(u_cameraPos, v_position);
	float factor = clamp((dist - u_fogStart) / u_fogRange, 0.0, 1.0);

	vec3 terrainColor = (grass * blend_map.x + dirt  * blend_map.y  + rock * blend_map.z) / (blend_map.x + blend_map.y + blend_map.z);
	gl_FragColor = vec4(mix(terrainColor, u_fogColor, factor), 1.0); 
}
