precision mediump float;
uniform sampler2D u_tex0;
uniform vec3 u_cameraPos;

varying vec2 v_texcoord;
varying vec3 v_position;
varying vec3 v_normal;

// ----- MATERIAL -----------
uniform vec4 u_m_diffuse;
uniform vec4 u_m_specular;

// --------------------------
//----- LIGHT ---------------
uniform vec4 lightDiffuse[3];
uniform vec4 lightSpecular[3];
uniform vec4 lightPosition[3];

uniform int lightsCount;
//---------------------------
uniform float AmbientWeight;
uniform vec4 AmbientGlobal;
uniform float specularPower;

void main()
{
	vec4 color = texture2D(u_tex0, v_texcoord);

	vec4 totalDiffuse = vec4(0.0, 0.0, 0.0, 0.0);
	vec4 totalSpecular = vec4(0.0, 0.0, 0.0, 0.0);
	vec3 toEye = normalize(u_cameraPos - v_position);
	vec3 normal = normalize(v_normal);

	for(int i = 0; i < lightsCount; i++)
	{
		vec4 diffuse = u_m_diffuse * lightDiffuse[i];
		vec4 specular = u_m_specular * lightSpecular[i];
		vec3 direction = lightPosition[i].w == 1.0 ? normalize(v_position - lightPosition[i].xyz) : lightPosition[i].xyz;
		
		vec4 diffuseComponent = max(dot(normal, -direction), 0.0)* diffuse;
		totalDiffuse += diffuseComponent;

		vec3 reflectVector = normalize(reflect(direction, normal));
		
		vec4 specularComponent = pow(max(dot(reflectVector, toEye), 0.0), specularPower)* specular;
		totalSpecular += specularComponent;
	}

	gl_FragColor = vec4(((AmbientGlobal * AmbientWeight + totalDiffuse * (1.0 - AmbientWeight)) * color + totalSpecular).xyz, color.w);
}
