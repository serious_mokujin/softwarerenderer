precision mediump float;
uniform sampler2D u_tex0;

varying vec2 v_texcoord;

void main()
{
	vec3 color = texture2D(u_tex0, v_texcoord).xyz;
	float l = 0.3 * color.r + 0.59 * color.g + 0.11 * color.b;
	gl_FragColor = vec4(vec3(l), 1.0);
}