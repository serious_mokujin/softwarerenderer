uniform mat4 u_ModelViewProjMatrix;
uniform mat4 u_modelMatrix;
uniform vec3 u_cameraPos;

attribute vec3 a_pos;
attribute vec3 a_normal;
//attribute vec2 a_texcoord;

varying vec3 v_reflection;

void main()
{
	vec3 position = vec4(u_modelMatrix * vec4(a_pos, 1.0)).xyz;
	vec3 normal = mat3(u_modelMatrix) * a_normal;
	vec3 toEye = u_cameraPos - position;
	v_reflection = reflect(normalize(-toEye), normalize(normal));
	gl_Position = u_ModelViewProjMatrix *  vec4(a_pos, 1.0);
}