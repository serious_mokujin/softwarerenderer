precision mediump float;
uniform sampler2D u_tex0;

uniform float limit;

varying vec2 v_texcoord;

void main(void)
{
	vec3 color = texture2D(u_tex0, v_texcoord).rgb;
	float brightness = 0.3 * color.r + 0.59 * color.g + 0.11 * color.b;
	float val = step(limit, brightness);
	gl_FragColor = vec4(color * val, 1.0);
}