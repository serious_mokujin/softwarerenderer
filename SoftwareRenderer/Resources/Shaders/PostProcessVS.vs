attribute vec2 a_pos;

varying vec2 v_texcoord;

void main()
{
	v_texcoord = (vec2( a_pos.x,  a_pos.y ) + vec2(1.0) ) / vec2(2.0);
	gl_Position = vec4(a_pos, 0.0, 1.0);
}
