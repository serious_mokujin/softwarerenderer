precision mediump float;
uniform samplerCube u_cubtex0;
uniform vec3 u_cameraPos;

varying vec3 v_position;

void main()
{
	//vec3 dir = v_position-u_cameraPos;
	gl_FragColor = textureCube(u_cubtex0, v_position); 
}
