precision mediump float;
uniform sampler2D u_tex0;

varying vec2 v_texcoord;

void main()
{
	gl_FragColor = texture2D(u_tex0, v_texcoord);
}
