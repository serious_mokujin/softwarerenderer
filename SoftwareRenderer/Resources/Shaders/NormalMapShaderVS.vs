uniform mat4 u_ModelViewProjMatrix;
uniform mat4 u_modelMatrix;

attribute vec3 a_pos;
attribute vec3 a_normal;
attribute vec2 a_texcoord;
attribute vec3 a_binormal;
attribute vec3 a_tangent;

varying vec3 v_normal;
varying vec3 v_position;
varying vec2 v_texcoord;
varying vec3 v_binormal;
varying vec3 v_tangent;

void main()
{
	v_position = vec4(u_modelMatrix * vec4(a_pos, 1.0)).xyz;
	v_normal = mat3(u_modelMatrix) * a_normal;
	v_texcoord = a_texcoord;
	v_binormal = mat3(u_modelMatrix) * a_binormal;
	v_tangent = mat3(u_modelMatrix) * a_tangent;
	gl_Position = u_ModelViewProjMatrix *  vec4(a_pos, 1.0);
}