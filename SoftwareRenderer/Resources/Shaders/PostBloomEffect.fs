precision mediump float;
uniform sampler2D u_tex0;
uniform sampler2D u_tex1;

varying vec2 v_texcoord;
uniform float multiplier;

void main(void)
{
	vec4 color = texture2D(u_tex0, v_texcoord);
	vec4 blur = texture2D(u_tex1, v_texcoord);
	gl_FragColor = color + multiplier * blur;
}